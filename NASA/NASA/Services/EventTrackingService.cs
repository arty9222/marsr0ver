﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NASA.Models;

namespace NASA.Services
{
    public class EventTrackingService<T> where T : MovementBaseEvent
    {
        private IList<T> _eventLogger = new List<T>();

        public void TrackEvent(T e)
        {
            _eventLogger.Add(e);
        }

        public List<T> GetEvents()
        {
            return _eventLogger as List<T>;
        }
    }
}
