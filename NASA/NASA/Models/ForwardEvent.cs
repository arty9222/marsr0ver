﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NASA.Models
{
    /// <summary>
    /// Store of an event capturing new position, direction and boundary at that point in time
    /// </summary>
    public class ForwardEvent : MovementBaseEvent
    {
        public ForwardEvent(DateTime timestamp, Guid guid, Plateau plateau) : base(timestamp, guid, plateau)
        {
        }
    }
}
