﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NASA.Models
{
    public class TurnEvent: MovementBaseEvent
    {
        public TurnEvent(DateTime timestamp, Guid guid, Plateau plateau, char directionCode) : base(timestamp, guid, plateau)
        {
            _direction = directionCode;
            
        }
        public char _direction { get; set; }
    }
}
