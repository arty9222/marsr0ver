﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NASA.Models
{
    public abstract class MovementBaseEvent
    {
        public MovementBaseEvent(DateTime timestamp, Guid guid, Plateau plateau)
        {
            _timestamp = timestamp;
            _guid = guid;
            _plateau = plateau;
        }
        public Guid _guid { get; set; }
        public DateTime _timestamp { get; set; }
        public Plateau _plateau { get; set; }
    }
}
