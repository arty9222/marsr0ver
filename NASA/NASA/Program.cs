﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NASA.Models;
using NASA.Services;
// Modified from original: https://github.com/sazarag/Mars-Rover
// 13/03/2019
// Added EventSourcing
namespace NASA
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Setup Rovers and Tracker
            EventTrackingService<MovementBaseEvent> movementTracker = new EventTrackingService<MovementBaseEvent>();
            RoboticRover firstRover = new RoboticRover(Guid.NewGuid(),0, 0, Direction.N, new Plateau(5, 5), movementTracker);
            RoboticRover secondRover = new RoboticRover(Guid.NewGuid(), 0, 0, Direction.N, new Plateau(5, 5), movementTracker);

            //Welcome message on Terminal
            Console.WriteLine("Welcome Earthling.You have two Rovers at your disposal. Send instructions as per below:" + Environment.NewLine);
            Console.WriteLine("L        Spin 90 degrees left without moving from spot" + Environment.NewLine);
            Console.WriteLine("R        Spin 90 degrees left without moving from spot" + Environment.NewLine);
            Console.WriteLine("M        Move forward one grid spot" + Environment.NewLine);

            while (1 == 1) //always online
            {
                firstRover.Command(Console.ReadLine());
                secondRover.Command(Console.ReadLine());
                firstRover.GetPosition();
                secondRover.GetPosition();
            }
        }
    }
}
