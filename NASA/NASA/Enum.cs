﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NASA
{
    /// <summary>
    /// Shows the direction of the rover 
    /// </summary>
    public enum Direction
    {
        N = 90,
        E = 180,
        S = 270,
        W = 360
    }
    static partial class Extensions
    {

        public static Direction ToEnum(char directionCode)
        {
            switch (directionCode)
            {
                case 'N':
                    return Direction.N;
                case 'E':
                    return Direction.E;
                case 'S':
                    return Direction.S;
                case 'W':
                    return Direction.W;
                default :
                    return Direction.N;
            }
        }
        public static char ToChar(Direction direction)
        {
            switch (direction)
            {
                case Direction.N:
                    return 'N';
                case Direction.E:
                    return 'E';
                case Direction.S:
                    return 'S';
                case Direction.W:
                    return 'W';
                default:
                    return 'N';
            }
        }
    }
}
