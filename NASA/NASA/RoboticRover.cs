﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NASA.Models;
using NASA.Services;
namespace NASA
{
    public class RoboticRover
    {
        private Guid _guid {get; set; }
        private int _positionX { get; set; }
        private int _positionY { get; set; }
        private Direction _direction { get; set; }
        private Plateau _plateau { get; set; }
        private EventTrackingService<MovementBaseEvent> _eventTrackingService { get; set; }

        public int positionX
        {
            get { return _positionX; }
            set { _positionX = value; }
        }
        public int positionY
        {
            get { return _positionY; }
            set { _positionY = value; }
        }
        public Direction direction
        {
            get { return _direction; }
            set { _direction = value; }
        }
        public Plateau plateau
        {
            get { return _plateau;  }
            set { _plateau = value; }
        }

        /// <summary>
        /// Constructor of the class 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="direction"></param>
        /// <param name="plateau"></param>
        public RoboticRover(Guid guid,Int32 x, Int32 y, Direction direction, Plateau plateau, EventTrackingService<MovementBaseEvent> eventTrackingService)
        {
            _guid = guid;
            _positionX = x;
            _positionY = y;            
            _direction = direction;
            _plateau = plateau;
            _eventTrackingService = eventTrackingService;
        }

        /// <summary>
        /// Movements of rover based on direction
        /// </summary>
        private void Go()
        {
            if (direction == Direction.N && plateau.Y > positionY)
            {                
                positionY++;
            }
            else if (direction == Direction.E && plateau.X > positionX)
            {
                positionX++;                
            }
            else if (direction == Direction.S && positionY > 0)
            {                
                positionY--;
            }
            else if (direction == Direction.W && positionX > 0)
            {
                positionX--;                
            }
            HandleEvent(new ForwardEvent(DateTime.Now, _guid, plateau));
        }

        /// <summary>
        ///  Event handler for tracking movements
        /// </summary>
        /// <param name="ev"></param>
        private void HandleEvent(MovementBaseEvent ev)
        {
            _eventTrackingService.TrackEvent(ev);
        }

        /// <summary>
        /// Change the direction of the rover
        /// </summary>
        /// <param name="directionCode"></param>
        private void ChangeDirection(Char directionCode)
        {
            if (directionCode == 'L')
                direction = (_direction - 90) < Direction.N ? Direction.W : direction - 90;
            else if(directionCode == 'R')
                direction = (_direction + 90) > Direction.W ? Direction.N : direction + 90;

            HandleEvent(new TurnEvent(DateTime.Now, _guid, plateau, directionCode));
        }

        /// <summary>
        /// Process the command string
        /// </summary>
        /// <param name="commandStr"></param>
        public void Command(string commandStr)
        {
            foreach (var command in commandStr)
            {
                if (command == 'L' || command == 'R')
                    ChangeDirection(command);
                else if(command == 'M') 
                    Go();
            }            
        }

        /// <summary>
        /// Get position and direction of the cover at the end of the command
        /// </summary>
        public string GetPosition()
        {
            string printedRoverPosition = string.Format("{0} {1} {2}", _positionX, _positionY, _direction);
            Console.WriteLine(printedRoverPosition);
            return printedRoverPosition;
        }

        /// <summary>
        /// Replay movement events up to a timestamp. Return new computed position at the end of the command replay
        /// </summary>
        public string GetPositionAtTimestamp(DateTime timestamp)
        {
            ResetRoverNavigation();
            foreach (MovementBaseEvent me in _eventTrackingService.GetEvents().Where(ev => ev._guid == _guid && ev._timestamp <= timestamp)) {
                if (me.GetType() == typeof(ForwardEvent)) Go();
                if (me.GetType() == typeof(TurnEvent)) ChangeDirection((me as TurnEvent)._direction);
            }
            string printedRoverPosition = string.Format("{0} {1} {2}", positionX, positionY, direction);
            Console.WriteLine(printedRoverPosition);
            return printedRoverPosition;
        }

        /// <summary>
        /// Restores Rover to original initialisation
        /// </summary>
        public void ResetRoverNavigation()
        {
            direction = Direction.N;
            positionX = 0;
            positionY = 0;
            plateau = new Plateau(5, 5);
        }
    }
}
