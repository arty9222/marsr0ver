using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using NASA;
using NASA.Models;
using NASA.Services;

namespace NASA.Test
{
    [TestFixture]
    public class TestFixture
    {
        private RoboticRover firstRover = null;
        private RoboticRover secondRover = null;
        private EventTrackingService<MovementBaseEvent> movementTracker = new EventTrackingService<MovementBaseEvent>();

        /// <summary>
        /// Set the first roboticRover
        /// </summary>
        private void SetupFirstRover()
        {

            firstRover = new RoboticRover(new Guid(), 1, 2, Direction.N, new Plateau(5, 5), movementTracker);
        }

        /// <summary>
        /// Set the second roboticRover
        /// </summary>
        private void SetupSecondRover()
        {
            EventTrackingService<MovementBaseEvent> movementTracker = new EventTrackingService<MovementBaseEvent>();
            secondRover = new RoboticRover(new Guid(), 3, 3, Direction.E, new Plateau(5, 5), movementTracker);
        }

        /// <summary>
        /// Correct assertion 
        /// </summary>
        [Test]
        public void MoveFirstRover()
        {
            SetupFirstRover();
            firstRover.Command("LMLMLMLMM");
            Assert.AreEqual(firstRover.GetPosition(), "1 3 N");
        }

        /// <summary>
        /// Wrong assertion
        /// </summary>
        [Test]
        public void MoveSecondRover()
        {
            SetupSecondRover();
            secondRover.Command("MMRMMRMRRM");
            Assert.AreEqual(secondRover.GetPosition(), "1 3 N");
            //Assert.AreEqual(secondRover.GetPosition(), "5 1 E");
        }

        /// <summary>
        /// Can't go out 
        /// </summary>
        [Test]
        public void BlurringTheBoundaries()
        {
            SetupSecondRover();
            secondRover.Command("MMMMMMMMMM");
            Assert.AreEqual(secondRover.GetPosition(), "5 3 E");
        }

        /// <summary>
        /// Verify Event Sourcing works
        /// </summary>
        [TestCase]
        public void SequenceChecking()
        {

        }
    }
}
